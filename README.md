# Influxdb + Grafana + Compose

Start service with:

```
docker-compose up -d
```

It is prepared to bring up on boot.

Influxdb uses as volume /mnt/sda/influxdb. You will need to previously mount this drive.
